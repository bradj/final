import config
import sources


def ampli(sound,factor:float):#amplificar el sonido
    for x in range(len(sound)):
        if sound[x] * factor >= config.max_amp:
            sound[x]= config.max_amp
        elif sound[x] * factor <= -config.max_amp:
            sound[x] = -config.max_amp
        elif sound[x] *factor < config.max_amp and sound [x] *factor > -config.max_amp:
            sound[x]=int (sound[x]*factor)

    return sound


def reduce(sound, factor: int):
    deletesound= sound[0::factor]
    while len(deletesound) != 0:
        for x in sound:
            if x in deletesound:
                sound.remove(x)
                deletesound.remove(x)

    return sound

def extend(sound, factor:int):
    if factor > 0:
        for x in range (len(sound)):
            x +=1
            if x % factor == 0:
                nuevo = ((sound[x] + sound [x+1])//2)
                sound.insert(x, nuevo)


